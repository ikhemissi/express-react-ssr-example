module.exports = {
  environment: 'default',
  server: {
    port: process.env.PORT || 9000,
  },
  logger: {
    simple: false,
    enabled: true,
  },
};
