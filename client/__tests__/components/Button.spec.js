import React from 'react';
import renderer from 'react-test-renderer';
import Button from '../../components/Button';

describe('Button', () => {
  it('should render a link', () => {
    const component = renderer.create(
      <Button href="/target">Label</Button>,
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
