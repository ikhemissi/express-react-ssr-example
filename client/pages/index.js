import React, { Component } from 'react';
import axios from 'axios';
import Products from '../components/Products';
import Error from '../components/Error';

class Landing extends Component {
  static async getInitialProps() {
    try {
      const isServer = typeof window === 'undefined';
      const productsEndpoint = isServer ? 'http://localhost:9000/api/products' : '/api/products';
      const response = await axios.get(productsEndpoint);
      const products = response.data;

      return { products };
    } catch (error) {
      return { products: [], error: error.message };
    }
  }

  render() {
    // eslint-disable-next-line react/prop-types
    const { products, error } = this.props;

    if (error) {
      return <Error message={error.message} />;
    }

    return (
      <div>
        <h1>Products</h1>
        <Products list={products} />
      </div>
    );
  }
}

export default Landing;
