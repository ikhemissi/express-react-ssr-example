import React from 'react';
import Button from '../components/Button';

const About = () => (
  <div>
    <h1>Company + team + product</h1>
    <Button href="/">Home</Button>
  </div>
);

export default About;
