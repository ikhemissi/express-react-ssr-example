import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Image extends Component {
  constructor(props) {
    super(props);
    this.state = {
      source: props.src,
      label: props.alt,
    };
    this.handleImageLoadingError = this.handleImageLoadingError.bind(this);
  }

  handleImageLoadingError() {
    const { fallback } = this.props;

    this.setState({ source: fallback });
  }

  render() {
    const { source, label } = this.state;

    return (
      <img src={source} alt={label} onError={this.handleImageLoadingError} />
    );
  }
}

Image.defaultProps = {
  fallback: '/static/notfound.png',
};

Image.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  fallback: PropTypes.string,
};

export default Image;
