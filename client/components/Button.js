import React from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Label = styled.span`
font-size: 20px;
font-weight: bold;
border: 3px solid green;
padding: 10px;
margin: 5px;
text-align: center;
cursor: pointer;
display: block;
`;

const Button = ({ children, ...rest }) => (
  <Link {...rest}>
    <Label>
      {children}
    </Label>
  </Link>
);

Button.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
};

export default Button;
