import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from './Button';
import Image from './Image';

const unit = 5;
const Card = styled.div`
  padding: ${2 * unit}px;
  border: 1px solid #888888;

  img {
    width: 100%;
    height: 200px;
    object-fit: cover;
  }

  p {
    height: 100px;
  }

  .line {
    display: block;
    width: 100%;
  }

  .price {
    font-weight: bold;
    margin-left: 5px;
    font-size: 1.5em;
  }
`;

const CardContents = styled.div`
  padding: ${unit}px;
`;

const CardActions = styled.div`
  display: block;
  width: 100%;
`;

const ProductLabel = styled.div`
  height: ${3 * unit}px;
  margin-top: ${2 * unit}px;

  span {
    background-color: red;
    color: white;
    padding: ${unit}px ${2 * unit}px;
    margin-left: -${3 * unit}px;
    font-weight: bold;
  }
`;

const decimals = {
  $: 2,
  '£': 2,
};

// eslint-disable-next-line object-curly-newline
const Product = ({ title, description, image, price, currency, priceLabel, productLabel, cta, ctaLink }) => {
  const decimalsInCurrency = decimals[currency];
  const displayPrice = (price / (10 ** decimalsInCurrency)).toFixed(decimalsInCurrency);

  return (
    <Card>
      <CardContents>
        <Image src={image.path} alt={image.alt || title} />
        <ProductLabel>
          {productLabel && <span className="product-label">{productLabel}</span>}
        </ProductLabel>
        <h2>{title}</h2>
        <p>{description}</p>
        <div className="line">
          {priceLabel}
          <span className="price">
            {currency}
            {displayPrice}
          </span>
        </div>
      </CardContents>
      <CardActions>
        <Button href={ctaLink}>{cta}</Button>
      </CardActions>
    </Card>
  );
};

Product.defaultProps = {
  cta: 'Add to cart',
  ctaLink: '#',
};

Product.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  image: PropTypes.shape({
    path: PropTypes.string,
    alt: PropTypes.string,
  }).isRequired,
  price: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired,
  priceLabel: PropTypes.string.isRequired,
  productLabel: PropTypes.string.isRequired,
  cta: PropTypes.string,
  ctaLink: PropTypes.string,
};

export default Product;
