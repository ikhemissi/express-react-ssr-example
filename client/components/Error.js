import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
  color: red;
`;

const Error = ({ message }) => (
  <Container>
    {message}
  </Container>
);

Error.defaultProps = {
  message: 'Error',
};

Error.propTypes = {
  message: PropTypes.string,
};

export default Error;
