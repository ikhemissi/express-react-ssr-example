import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Product from './Product';

const ListOfProducts = styled.div`
  display: grid;
  grid-gap: 20px;

  @media (min-width: 480px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (min-width: 768px) {
    grid-template-columns: repeat(3, 1fr);
  }
`;

const Products = ({ list }) => (
  <ListOfProducts>
    {list.map(product => <Product {...product} key={product.title} />)}
  </ListOfProducts>
);

Products.defaultProps = {
  list: [],
};

Products.propTypes = {
  list: PropTypes.arrayOf(PropTypes.shape({ ...Product.propTypes })),
};

export default Products;
