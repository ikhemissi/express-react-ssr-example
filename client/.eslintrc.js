module.exports = {
  extends: 'airbnb',
  env: {
    browser: true,
  },
  rules: {
    'react/jsx-filename-extension': 0,
    'object-curly-newline': 0,
    'max-len': [ 'error', { code: 200 }],
  },
};
