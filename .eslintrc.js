module.exports = {
  root: true,
  extends: 'airbnb-base',
  env: {
    node: true,
  },
  rules: {
    'object-curly-newline': 0,
    'max-len': [ 'error', { code: 200 }],
  }
};
