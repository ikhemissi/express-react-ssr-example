const config = require('config');
const express = require('express');
const cors = require('cors');
const compression = require('compression');
const helmet = require('helmet');
const api = require('./api');
const ssr = require('./ssr');
const logger = require('./utils/logger');

const server = express();

server
  .disable('x-powered-by')
  .disable('etag')
  .use(helmet())
  .use(cors())
  .use(compression())
  .use('/api', api)
  .use('/', ssr);

process.on('exit', () => {
  logger.error('Received EXIT');
});

process.on('SIGINT', () => {
  logger.error('Received SIGINT');
  process.exit(2);
});

server.listen(config.server.port, (error) => {
  if (error) {
    logger.error(error);
    process.exit(1);
  }

  logger.info('Server started', {
    environment: config.environment,
    port: config.server.port,
  });
});
