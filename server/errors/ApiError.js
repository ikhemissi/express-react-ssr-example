class ApiError extends Error {
  constructor({ message, statusCode, originalError }) {
    super(message);
    this.originalError = originalError;
    this.statusCode = statusCode || 500;
  }
}

module.exports = ApiError;
