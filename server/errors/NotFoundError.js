const ApiError = require('./ApiError');

class NotFoundError extends ApiError {
  constructor(message = 'Not found!') {
    super({
      message,
      statusCode: 404,
    });
  }
}

module.exports = NotFoundError;
