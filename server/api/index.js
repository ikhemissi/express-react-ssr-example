const { Router } = require('express');
const health = require('./health');
const products = require('./products');

const router = new Router();

router.use('/health', health);
router.use('/products', products);

module.exports = router;
