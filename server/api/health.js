const { Router } = require('express');
const config = require('config');

const router = new Router();

router.get('/check', (_, res) => {
  res.send({
    server: 'ok',
    environment: config.environment,
    uptime: process.uptime(),
    nodejs: process.version,
    process: {
      platform: process.platform,
      memory: process.memoryUsage(),
      pid: process.pid,
      cpuUsage: process.cpuUsage(),
      cwd: process.cwd(),
      arch: process.arch,
    },
  });
});

module.exports = router;
