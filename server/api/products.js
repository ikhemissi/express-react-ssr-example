const { Router } = require('express');
const logger = require('../utils/logger');
const NotFoundError = require('../errors/NotFoundError');
const products = require('../products');

const router = new Router();

router.get('/:productId', async (req, res) => {
  const { productId } = req.params;

  try {
    const product = products.getProduct(productId);

    if (!product) {
      throw new NotFoundError();
    }

    res.send(product);
  } catch (error) {
    logger.error(error);
    res.status(error.statusCode || 500).send({ message: error.message });
  }
});

router.get('/', async (_, res) => {
  try {
    const allProducts = products.getAllProducts();

    res.send(allProducts);
  } catch (error) {
    logger.error(error);
    res.status(error.statusCode || 500).send({ message: error.message });
  }
});

module.exports = router;
