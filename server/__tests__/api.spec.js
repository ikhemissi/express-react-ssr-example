
const request = require('supertest');
const express = require('express');
const apiRoutes = require('../api');
const products = require('../products/list.json');

const app = express();
const api = app.use('/api', apiRoutes);

describe('api', () => {
  it('should return all products', async () => {
    const server = request(api);

    return server
      .get('/api/products')
      .set('Accept', 'application/json')
      .expect(200)
      .expect((response) => {
        expect(response.body).toEqual(products);
      })
      .then();
  });

  it('should return a single product details', async () => {
    const server = request(api);
    const firstProduct = products[0];

    return server
      .get(`/api/products/${firstProduct.id}`)
      .set('Accept', 'application/json')
      .expect(200)
      .expect((response) => {
        expect(response.body).toEqual(firstProduct);
      })
      .then();
  });

  it('should return a 404 if the product does not exist', async () => {
    const server = request(api);

    return server
      .get('/api/products/unknown')
      .set('Accept', 'application/json')
      .expect(404, {
        message: 'Not found!',
      })
      .then();
  });
});
