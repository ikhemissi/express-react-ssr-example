const path = require('path');
const next = require('next');
const config = require('config');
const { Router } = require('express');
const { parse } = require('url');

const router = new Router();
const ssr = next({
  dev: config.environment === 'development',
  dir: path.join(__dirname, '../../client'),
});
const ssrHandler = ssr.getRequestHandler();
const ssrInit = ssr.prepare();

router.get('*', async (req, res) => {
  await ssrInit;

  const parsedUrl = parse(req.url, true);
  ssrHandler(req, res, parsedUrl);
});

module.exports = router;
