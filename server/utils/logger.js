const config = require('config');
const winston = require('winston');

const transports = [
  new (winston.transports.Console)(),
];

let formatter = winston.format.combine(
  winston.format.timestamp(),
  winston.format.json(),
);

if (config.logger.simple) {
  formatter = winston.format.combine(
    winston.format.colorize(),
    winston.format.simple(),
  );
}

const logger = winston.createLogger({ transports, format: formatter });

function error(errorInstance) {
  if (config.logger.enabled) {
    logger.error(errorInstance);
  }
}

function info(message, context) {
  if (config.logger.enabled) {
    logger.info(message, context);
  }
}

module.exports = {
  error,
  info,
};
