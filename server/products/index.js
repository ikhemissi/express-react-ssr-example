const catalog = require('./list.json');

function getAllProducts() {
  return catalog;
}

function getProduct(id) {
  return catalog.find(product => product.id === id);
}

module.exports = {
  getAllProducts,
  getProduct,
};
