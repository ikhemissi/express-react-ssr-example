# express-react-ssr-example

Example of an application with Express, React, and SSR with Next

In this project, the server also serves static assets and server side rendered pages, which is fine for a demo application.
For production though, I recommend using separate instances for static assets and ssr to have cleaner code and to better use available resources. 

## Running locally

```sh
# Install dependencies
npm install

# Lint
npm run lint

# Test
npm test

# Start the service on port 9000
npm start
```