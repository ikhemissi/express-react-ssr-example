FROM node:10.5.0

WORKDIR /app

COPY package.json package.json
RUN npm install
COPY . .

EXPOSE 9000

CMD [ "node", "./server/index.js" ]
